Gem::Specification.new do |spec|
  spec.name          = 'cappy'
  spec.version       = '0.0.0'
  spec.authors       = ['Simone Miller']
  spec.required_ruby_version = ">= 2.0.0"
  spec.summary       = "A library that capitalises text"
  spec.files         = ["lib/cappy.rb"]
end